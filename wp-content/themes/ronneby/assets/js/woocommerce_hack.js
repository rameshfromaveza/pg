var add_to_cart_button;

(function($){
	"use strict";
	
	var dfd_WC = window.dfd_WC || {};
	
	window.dfd_WC = dfd_WC;
	
	dfd_WC.window = $(window);
	dfd_WC.document = $(document);
	dfd_WC.windowWidth = dfd_WC.window.width();
	
	var supports_html5_storage = ( 'sessionStorage' in window && window.sessionStorage !== null );
	if (supports_html5_storage) { sessionStorage.clear(); }
		
	/* Plus-minus buttons customization */
	var initArrows = function() {
		$('.single-product form.cart .quantity, .shop_table .quantity, .woocommerce-grouped-product-list-item__quantity .quantity').each(function(){
			var inputNumber, min, max, $self = $(this);
			if($self.length > 0) {
				$self.prepend('<i class="dfd-icon-down_2 minus">').append('<i class="dfd-icon-up_2 plus">');
				$self.find('.minus').unbind('click').bind('click', function() {
					inputNumber = $(this).siblings('.qty');
					min = inputNumber.attr('min');
					max = inputNumber.attr('max');
					var beforeVal = +inputNumber.val();
					var newVal = (beforeVal > min || !min) ? +beforeVal - 1 : min;
					inputNumber.val(newVal);
					$(this).parent().siblings('.single_add_to_cart_button').attr('data-quantity', newVal);
					if($('.coupon .dfd-submit-wrap .button').length > 0) {
						$('.coupon .dfd-submit-wrap .button').prop("disabled", false);
					}
				});
				$self.find('.plus').unbind('click').bind('click', function() {
					inputNumber = $(this).siblings('.qty');
					min = inputNumber.attr('min');
					max = inputNumber.attr('max');
					var beforeVal = +inputNumber.val();
					var newVal = (beforeVal < max || !max) ? +beforeVal + 1 : max;
					inputNumber.val(newVal);
					$(this).parent().siblings('.single_add_to_cart_button').attr('data-quantity', newVal);
					if($('.coupon .dfd-submit-wrap .button').length > 0) {
						$('.coupon .dfd-submit-wrap .button').prop("disabled", false);
					}
				});
			}
			$self.find('.qty').on('input propertychange',function() {
				$('.single_add_to_cart_button').attr('data-quantity', $(this).val());
			});
			if($('.wcmp-quick-view-wrapper').length > 0)
				$('.wcmp-quick-view-wrapper form.cart .single_add_to_cart_button').removeClass('product_type_simple');
		});
	};
	
	$(document).ready(function(){
		var wooInitDropkick = function() {
			if($('body').hasClass('single-product')) {
				if ($('.ul-dropdown-toggle').length>0)
					$('.ul-dropdown-toggle').dropkick({mobile: true});
				if ($('.variations .value select').length>0)
					$('.variations .value select').dropkick({mobile: true});
			}
			if($('body.woocommerce-cart .cart-wrap .shipping select').length > 0) {
				$('body.woocommerce-cart .cart-wrap .shipping select:not(#calc_shipping_country):not(#calc_shipping_state)').dropkick();
			}
		};
		if(!$('html').hasClass('dfd-ie-detected')) {
			wooInitDropkick();
			$('.cart-collaterals > .cover').observeDOM(function() {
				wooInitDropkick();
			});
			$('.variations .value select').observeDOM(function() {
				if ($('.variations .value select').length>0) {
					$('.variations .value select').dropkick('refresh');
				}
			});

			$('.variations_form').on('click touchend', '.dfd-reset-vars', function(e) {
				$('table.variations select').dropkick('reset', true);
			});
		}
		
		$('body').on('adding_to_cart', function(trigger, button) {
			add_to_cart_button = button;
		});
		
		$('body').on('added_to_cart', function (trigger) {
			if (add_to_cart_button != undefined) {
				var $woo_entry_thumb = $(add_to_cart_button).parents('li.product').find('div.woo-entry-thumb');
				var $added_to_cart_notice = $('<div class="added-to-cart-notice moon-checkmark">Added to cart</div>');
				
				if ($woo_entry_thumb.length > 0) {
					$woo_entry_thumb.append($added_to_cart_notice);
					$added_to_cart_notice.stop().animate({opacity: 1}, 800).delay( 1800 ).animate({opacity: 0}, 800, function() {$(this).remove()});
				}
				add_to_cart_button = null;
			}
		});
		
		initArrows();
		
		var $toggleVariationsButton = $('.single-product .variations_form .variations_button');
		$('body').on('hide_variation', function () {
			if($toggleVariationsButton.length > 0) {
				$toggleVariationsButton.slideUp(200);
			}
		});
		$('body').on('show_variation', function () {
			if($toggleVariationsButton.length > 0) {
				$toggleVariationsButton.slideDown(200);
			}
		});
	});
	
	var wrap = $('body.single-product .product .woocommerce-product-gallery__wrapper > .woocommerce-product-gallery__image:first-child');
	var wrapLeft, wrapTop, wrapWidth, wrapHeight, largeImage, largeImageWidth, largeImageheight, ratioX, ratioY, largeImageSrc;
	var generateLargeImage = function() {
		largeImageSrc = wrap.find('.wp-post-image').data('large_image');
		wrap.find('> a').append('<img src="' + largeImageSrc + '" alt="" class="dfd-large-image" />');
	};
	
	var calculateVars = function() {
		setTimeout(function() {
			wrapLeft =  wrap.offset().left;
			wrapTop =  wrap.offset().top;
			wrapWidth =  wrap.width();
			wrapHeight =  wrap.height();
			largeImage = wrap.find('img.dfd-large-image');
			largeImageWidth = largeImage.width();
			largeImageheight = largeImage.height();
			ratioX = largeImageWidth / wrapWidth - 1;
			ratioY = largeImageheight / wrapHeight - 1;
		},100);
	};
	var magnifierMove = function() {
		wrap.mousemove(function(e) {
			if(largeImage) {
				var coordLeft = (e.pageX - wrapLeft) * ratioX;
				if(coordLeft < 0) coordLeft = 0;
				if(coordLeft > largeImageWidth) coordLeft = largeImageWidth;
				var coordTop = (e.pageY - wrapTop) * ratioY;
				if(coordTop < 0) coordTop = 0;
				if(coordTop > largeImageheight) coordTop = largeImageheight;
				largeImage.css({
					'left' : -coordLeft,
					'top' : -coordTop
				});
			}
		});
	};
	
	if(wrap.length > 0) {
		dfd_WC.document.on('ready', magnifierMove);
		dfd_WC.window.on("resize load scroll", calculateVars);
	}
	
	dfd_WC.products_li_eq_height = function() {
		jQuery('.products.row').each(function() {
			$(this).find('.product').equalHeights();
		});
	};

	dfd_WC.changeProdImageVariable = function() {
		var $productContainer = $('.single-product.woocommerce #layout .product.type-product'),
			$trigerImages = $productContainer.find('.images');
		
		$trigerImages.on('woocommerce_gallery_reset_slide_position', function() {
			var $src = $productContainer.find('.woocommerce-product-gallery__wrapper > .woocommerce-product-gallery__image a').attr('href');
			$productContainer.find('.dfd-large-image').attr('src', $src);
			dfd_WC.window.resize();
		});
	};

	dfd_WC.singleProdThumbCaroucel = function() {
		var $productContainer = $('.single-product.woocommerce #layout .product.type-product'),
			$container = $productContainer.find('.single-product-thumbnails'),
			$horisontal = false,
			$html,
			$i = 0,
			$dataThumbFirst;
		
		if($productContainer.find('.dfd-single-product-media-wrap').hasClass('thumbs-left')) {
			$horisontal = true;
		}
		
		$dataThumbFirst = $productContainer.find('.woocommerce-product-gallery__wrapper > .woocommerce-product-gallery__image').data('thumb');
		
		$productContainer.find('.woocommerce-product-gallery__wrapper > .woocommerce-product-gallery__image a').attr('data-rel', 'prettyPhoto[woo_single_gal]').attr('data-thumb', $dataThumbFirst).addClass('prettyPhoto');
		
		$productContainer.find('.dfd-single-product-thumbnails-wrap').append('<div class="hide"></div>');
		
		$container.find('.woocommerce-product-gallery__image').each(function() {
			$i++;
			if( $i === 1 ) {
				return;
			}
			
			var $self = $(this),
				$img = $self.find('img'),
				$fullImg = $img.data('src'),
				$thumbImg = $img.attr('src');
			
			$html = '<a href="' + $fullImg + '" data-thumb="' + $thumbImg + '" data-rel="prettyPhoto[woo_single_gal]"></a>';
			$productContainer.find('.hide').append($html);
		});
		
		$('body').trigger('post-load');
		
		$container.products_thumbnails_carousel(4, $horisontal);
		$container.find('.woocommerce-product-gallery__image a').each(function() {
			var $this = $(this);
			$this.click(function(e) {
				e.preventDefault();
				var url = $this.attr('href');
				var urlFull = $this.find('img').attr('data-large_image');
				
				$this.parent().parents('.images').find('.woocommerce-product-gallery__wrapper img.wp-post-image').attr('src', url).attr('srcset', url);
				$productContainer.find('.woocommerce-product-gallery__wrapper img.dfd-large-image').attr('src', urlFull);
			});
		});
	};
	
	dfd_WC.initProductThumbCarousel = function() {
		var $carousel = $('.woo-entry-thumb-carousel'),
			speed = $carousel.data('speed');
		$carousel.slick({
			infinite: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			dots: false,
			fade: true,
			autoplay: true,
			autoplaySpeed: speed,
			pauseOnHover: false
		});
	};
	
	dfd_WC.window.on('load resize', function() {
		dfd_WC.products_li_eq_height();
	});
	
	dfd_WC.document.ready(function() {
		dfd_WC.products_li_eq_height();
		generateLargeImage();
		dfd_WC.changeProdImageVariable();
		dfd_WC.singleProdThumbCaroucel();
		dfd_WC.initProductThumbCarousel();
	});
	
	$(document.body).on('updated_wc_div cart_page_refreshed',function() {
		initArrows();
		dfd_WC.initProductThumbCarousel();
		$( document ).trigger('change input');
	});
	
})(jQuery);
