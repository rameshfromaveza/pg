<?php
if ( ! defined( 'ABSPATH' ) ) { exit; }
/*
@Module: List view
@Since: 1.0
@Package: WooComposer
*/
if(!class_exists("Dfd_Woo_Products_List")) {
	class Dfd_Woo_Products_List	{
		
		function __construct(){
			add_shortcode('dfd_woo_list', array($this, 'woocomposer_list_shortcode'));
			add_action('init', array($this, 'woocomposer_init_grid'));
		}
		
		function woocomposer_init_grid(){
			if(function_exists('vc_map')){
				vc_map(
					array(
						'name' => esc_html__('Product List', 'dfd'),
						'base' => 'dfd_woo_list',
						'icon' => 'dfd_woo_list dfd_shortcode',
						'category' => __('WooComposer', 'dfd'),
						'description' => 'Display products in list view',
						'controls' => 'full',
						'show_settings_on_create' => true,
						'params' => array(
							array(
								'type' => 'ult_param_heading',
								'text' => esc_html__('General module settings', 'dfd'),
								'param_name' => 'general_heading',
								'edit_field_class' => 'ult-param-heading-wrapper no-top-margin vc_column vc_col-sm-12',
								//'group' => esc_html__('General', 'dfd'),
							),
							array(
								'type' => 'woocomposer',
								'heading' => esc_html__('Query Builder', 'dfd'),
								'param_name' => 'shortcode',
								'module' => 'list',
								'labels' => array(
									'products_from' => __('Display:', 'dfd'),
									'per_page' => __('How Many:', 'dfd'),
									'columns' => __('Columns:', 'dfd'),
									'order_by' => __('Order By:', 'dfd'),
									'order' => __('Display:', 'dfd'),
									'category' => __('Category:', 'dfd'),
								),
								//'group' => esc_attr__('General', 'dfd'),
							),
							array(
								'type'        => 'dropdown',
								'heading'     => __( 'Subheading content', 'dfd' ),
								'param_name'  => 'subheading_content',
								'value'       => array(
									__( 'Tags', 'dfd' )       => '',
									__( 'Product subtitle', 'dfd' )       => 'subtitle',
								),
								//'group'            => esc_attr__( 'General', 'dfd' ),
							),
							array(
								'type' => 'textfield',
								'heading' => __('Extra class name', 'js_composer'),
								'param_name' => 'el_class',
								//'group'            => esc_attr__( 'General', 'dfd' ),
								'description' => __('If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer')
							),
							array(
								'type'        => 'dropdown',
								'heading'     => __( 'Animation', 'dfd' ),
								'param_name'  => 'module_animation',
								'value'       => dfd_module_animation_styles(),
								//'group'            => esc_attr__( 'General', 'dfd' ),
							),
							array(
								'type'             => 'ult_param_heading',
								'text'             => esc_html__( 'General styling options', 'dfd' ),
								'param_name'       => 'stylle_main_heading',
								'group'            => esc_attr__( 'Style', 'dfd' ),
								'edit_field_class' => 'ult-param-heading-wrapper no-top-margin vc_column vc_col-sm-12',
							),
							array(
								'type'        => 'dropdown',
								'heading'     => __( 'Display style', 'dfd' ),
								'param_name'  => 'display_style',
								'value'       => array(
									__( 'Simple', 'dfd' )       => '',
									__( 'Menu mode', 'dfd' )       => 'menu-mode',
								),
								'group'            => esc_attr__( 'Style', 'dfd' ),
							),
							array(
								'type'             => 'ult_param_heading',
								'text'             => esc_html__( 'Title settings', 'dfd' ),
								'param_name'       => 'title_heading',
								'group'            => esc_attr__( 'Style', 'dfd' ),
								'edit_field_class' => 'ult-param-heading-wrapper vc_column vc_col-sm-12',
							),
							array(
								'type'       => 'dfd_font_container_param',
								'heading'    => '',
								'param_name' => 'title_font_options',
								'settings'   => array(
									'fields' => array(
										//'tag' => 'div',
										'letter_spacing',
										'font_size',
										'line_height',
										'color',
										'font_style'
									),
								),
								'group'            => esc_attr__( 'Style', 'dfd' ),
							),
							array(
								'type'        => 'checkbox',
								'heading'     => esc_html__( 'Use custom font family?', 'dfd' ),
								'param_name'  => 'title_google_fonts',
								'value'       => array( esc_html__( 'Yes', 'dfd' ) => 'yes' ),
								'description' => esc_html__( 'Use font family from google.', 'dfd' ),
								'group'            => esc_attr__( 'Style', 'dfd' ),
							),
							array(
								'type'       => 'google_fonts',
								'param_name' => 'title_custom_fonts',
								'value'      => '',
								'group'            => esc_attr__( 'Style', 'dfd' ),
								'settings'   => array(
									'fields' => array(
										'font_family_description' => esc_html__( 'Select font family.', 'dfd' ),
										'font_style_description'  => esc_html__( 'Select font styling.', 'dfd' ),
									),
								),
								'dependency' => array(
									'element' => 'title_google_fonts',
									'value'   => 'yes',
								),
							),
							array(
								'type'             => 'ult_param_heading',
								'text'             => esc_html__( 'Price settings', 'dfd' ),
								'param_name'       => 'price_heading',
								'group'            => esc_attr__( 'Style', 'dfd' ),
								'edit_field_class' => 'ult-param-heading-wrapper vc_column vc_col-sm-12',
							),
							array(
								'type'       => 'dfd_font_container_param',
								'heading'    => '',
								'param_name' => 'price_font_options',
								'settings'   => array(
									'fields' => array(
										//'tag' => 'div',
										'letter_spacing',
										'font_size',
										'line_height',
										'color',
										'font_style'
									),
								),
								'group'            => esc_attr__( 'Style', 'dfd' ),
							),
							array(
								'type'             => 'ult_param_heading',
								'text'             => esc_html__( 'Subtitle settings', 'dfd' ),
								'param_name'       => 'subtitle_heading',
								'group'            => esc_attr__( 'Style', 'dfd' ),
								'edit_field_class' => 'ult-param-heading-wrapper vc_column vc_col-sm-12',
							),
							array(
								'type'       => 'dfd_font_container_param',
								'heading'    => '',
								'param_name' => 'subtitle_font_options',
								'settings'   => array(
									'fields' => array(
										//'tag' => 'div',
										'letter_spacing',
										'font_size',
										'line_height',
										'color',
										'font_style'
									),
								),
								'group'            => esc_attr__( 'Style', 'dfd' ),
							),
						)/* vc_map params array */
					)/* vc_map parent array */ 
				); /* vc_map call */ 
			} /* vc_map function check */
		} /* end woocomposer_init_grid */
		function woocomposer_list_shortcode($atts){
			global $woocommerce, $dfd_ronneby;
			$shortcode = $subtitle_font = $subtitle_color = $module_animation = $display_style = $subheading_content = $new_shortcode = '';
			$title_font_options = $title_google_fonts = $title_custom_fonts = $price_font_options = $subtitle_font_options = '';
			
			$atts = vc_map_get_attributes( 'dfd_woo_list', $atts );
			extract( $atts );
			
			if (!isset($dfd_ronneby['dfd_woocommerce_templates_path']) || $dfd_ronneby['dfd_woocommerce_templates_path'] != '_old') {
				$el_class .= ' dfd-shop-new-styles';
			}
			
			$output = $on_sale = $style = $before_line = $after_line = $delim_html = '';
			if($display_style == 'menu-mode') {
				$before_line .= '<div class="clearfix dfd-list-menu-mode">';
				$after_line .= '</div>';
				$delim_html .= '';
			} else {
				$delim_html .= '-';
			}
			
			$animate = $animation_data = '';

			if ( ! ( $module_animation == '' ) ) {
				$animate        = ' cr-animate-gen';
				$animation_data = 'data-animate-type = "' . esc_attr($module_animation) . '" ';
			}
			
			$post_count = '12';
			$output .= '<div class="dfd-woocomposer_list woocommerce '.esc_attr($el_class).' '.esc_attr($animate).'" '.$animation_data.'>';
			/* $output .= do_shortcode($content); */
			$pattern = get_shortcode_regex();
			if($shortcode !== ''){
				$new_shortcode = rawurldecode( base64_decode( strip_tags( $shortcode ) ) );
			}
			preg_match_all("/".$pattern."/",$new_shortcode,$matches);
			$shortcode_str = str_replace('"','',str_replace(" ","&",trim($matches[3][0])));
			$short_atts = parse_str($shortcode_str, $str_output);//explode("&",$shortcode_str);
			extract($str_output);
			if(isset($matches[2][0])): $display_type = $matches[2][0]; else: $display_type = ''; endif;
			if(!isset($columns)): $columns = '4'; endif;
			if(isset($per_page)): $post_count = $per_page; endif;
			if(isset($number)): $post_count = $number; endif;
			if(!isset($order)): $order = 'ASC'; endif;
			if(!isset($orderby)): $orderby = 'date'; endif;
			if(!isset($category)): $category = ''; endif;
			if(!isset($ids)): $ids = ''; endif;
			if($ids){
				$ids = explode( ',', $ids );
				$ids = array_map( 'trim', $ids );
			}
			
			if($columns == "2") $columns = 6;
			elseif($columns == "3") $columns = 4;
			elseif($columns == "4") $columns = 3;
			$meta_query = '';
			$sticky = get_option('sticky_posts');

			$args = array(
				'post_type'             => 'product',
				'post_status'           => 'publish',
				'ignore_sticky_posts'   => 1,
				'posts_per_page'        => $post_count,
				'orderby'               => $orderby,
				'order'                 => $order,
			);
			switch($display_type) {
				case 'recent_products':
					$args['meta_query'] = WC()->query->get_meta_query();
					break;
				case 'featured_products':
					if(function_exists('wc_get_product_visibility_term_ids')) {
						$product_visibility_term_ids = wc_get_product_visibility_term_ids();
						$args['tax_query'][] = array(
							'taxonomy' => 'product_visibility',
							'field'    => 'term_taxonomy_id',
							'terms'    => $product_visibility_term_ids['featured'],
						);
					} else {
						$args['meta_query'] = array(
							array(
								'key' 		=> '_visibility',
								'value' 	  => array('catalog', 'visible'),
								'compare'	=> 'IN'
							),
							array(
								'key' 		=> '_featured',
								'value' 	  => 'yes'
							)
						);
					}
					break;
				case 'top_rated_products':
					$args['no_found_rows'] = 1;
					$args['meta_key'] = '_wc_average_rating';
					$args['orderby'] = 'meta_value_num';
					$args['order'] = 'DESC';
					$args['meta_query'] = WC()->query->get_meta_query();
					$args['tax_query'] = WC()->query->get_tax_query();
					break;
				case 'sale_products':
					global $woocommerce;
					if(function_exists('wc_get_product_ids_on_sale')) {
						$sale_product_ids = wc_get_product_ids_on_sale();
					} else {
						$sale_product_ids = woocommerce_get_product_ids_on_sale();
					}
					$meta_query = array();
					$meta_query[] = $woocommerce->query->visibility_meta_query();
					$meta_query[] = $woocommerce->query->stock_status_meta_query();
					$args['meta_query'] = $meta_query;
					$args['post__in'] = $sale_product_ids;
					break;
				case 'best_selling_products':
					$args['meta_key'] = 'total_sales';
					$args['orderby'] = 'meta_value_num';
					$args['meta_query'] = array(
							array(
								'key' 		=> '_visibility',
								'value' 	=> array( 'catalog', 'visible' ),
								'compare' 	=> 'IN'
							)
						);
					break;
				case 'product_categories':
					if (!empty($ids)) {
						$args['tax_query'] = array(
							array(
								'taxonomy' => 'product_cat',
								'field' => 'slug',
								'terms' => $ids,
							)
						);
					}
					break;
				case 'product_category':
					if (!empty($category)) {
						$args['tax_query'] = array(
							array(
								'taxonomy' 	 => 'product_cat',
								'terms' 		=> array( esc_attr( $category ) ),
								'field' 		=> 'name',
								'operator' 	 => 'IN'
							)
						);
					}
					break;
			}
			$query = new WP_Query( $args );
			$output .= '<ul class="dfd-woo-product-list '.$order.'">';
			if($query->have_posts()):
				while ( $query->have_posts() ) : $query->the_post();
			
					$title_html = $price_html = $subtitle_html = '';
			
					$product_id = get_the_ID();
					//$post = get_post($product_id);
					$product_title = get_the_title();
					
					$product = new WC_Product( $product_id );
					//$attachment_ids = $product->get_gallery_attachment_ids();
					$price = $product->get_price_html();
					if(function_exists('wc_get_rating_html')) {
						$rating = wc_get_rating_html($product->get_average_rating());
					} else {
						$rating = $product->get_rating_html();
					}
					//$product_var = new WC_Product_Variable( $product_id );
					//$available_variations = $product_var->get_available_variations();
					$cat_count = sizeof( get_the_terms( $product_id, 'product_cat' ) );
					
					$title_options = _crum_parse_text_shortcode_params( $title_font_options, '', $title_google_fonts, $title_custom_fonts );
					$title_html .= '<a href="'.get_permalink($product_id).'" class="box-name" '.$title_options['style'].'><span>'.$product_title.'</span></a>';
					
					$price_options = _crum_parse_text_shortcode_params( $price_font_options, '' );
					$price_html .= '<span class="amount" '.$price_options['style'].'>'.$price.'</span>';
					
					if($subheading_content == 'subtitle') {
						$subtitle = DfdMetaBoxSettings::get('dfd_product_product_subtitle');
					} else {
						if(function_exists('wc_get_product_category_list')) {
							$subtitle = wc_get_product_category_list($product_id, ', ','<span class="posted_in">'._n('','',$cat_count,'woocommerce').' ','.</span>');
						} else {
							$subtitle = $product->get_categories(', ','<span class="posted_in">'._n('','',$cat_count,'woocommerce').' ','.</span>');
						}
					}
					
					$subtitle_options = _crum_parse_text_shortcode_params( $subtitle_font_options, '' );
					$subtitle_html .= '<span class="subtitle" '.$subtitle_options['style'].'>'.$subtitle.'</span>';
				
						$output .= '<li>';
						
							$output .= $before_line;
							$output .= $title_html;
							$output .= '<span class="woo-delim">'.esc_html($delim_html).'</span>';
							$output .= $price_html;
							$output .= $after_line;
							$output .= $before_line;
							$output .= $subtitle_html;
							if($display_type == "top_rated_products"){						
								$output .= '<div>'.$rating.'</div>';
							}					
							$output .= $after_line;
					$output .= '</li>';
				endwhile;
			endif;
			$output .= "\n".'</ul>';
			$output .= "\n".'</div>';
			wp_reset_postdata();
			return $output;
		}/* end woocomposer_list_shortcode */
	}
	new Dfd_Woo_Products_List;
}
if(class_exists('WPBakeryShortCode'))
{
	class WPBakeryShortCode_dfd_woo_list extends WPBakeryShortCode {
	}
}
